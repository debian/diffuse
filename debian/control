Source: diffuse
Maintainer: Philipp Huebner <debalance@debian.org>
Section: editors
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               gettext,
               libglib2.0-dev,
               meson,
               python3-all
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/diffuse
Vcs-Git: https://salsa.debian.org/debian/diffuse.git
Homepage: https://github.com/MightyCreak/diffuse

Package: diffuse
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         gir1.2-gtk-3.0,
         gnome-icon-theme,
         python3-gi,
         python3-gi-cairo
Suggests: desktop-file-utils
Description: graphical tool for merging and comparing text files
 Diffuse is a graphical tool for merging and comparing text files. Diffuse is
 able to compare an arbitrary number of files side-by-side and gives users the
 ability to manually adjust line-matching and directly edit files. Diffuse can
 also retrieve revisions of files from bazaar, CVS, darcs, git, mercurial,
 monotone, Subversion and GNU Revision Control System (RCS) repositories for
 comparison and merging.
